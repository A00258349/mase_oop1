package week4;

public class PassReferences {
	static void f(PassReferences h) {
		System.out.println("h inside of f(): "+h);
	}
	public static void main(String[] args) {
		PassReferences p = new PassReferences();
		System.out.println("p inside of main(): "+p);
		f(p);

	}

}
