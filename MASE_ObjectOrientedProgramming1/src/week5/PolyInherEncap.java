
package week5;

class Human {
	public String whoAmI() {
		return "I am a human";
	}

//	@Override
//	public String toString() {
//		return "" + getClass();
//	}
}

class Male extends Human {
	public String show() {
		return "Show male";
	}

	@Override
	public String whoAmI() {
		return "I am a male";
	}

	@Override
	public String toString() {
		return "" + getClass();
	}
}

class Female extends Human {
	@Override
	public String whoAmI() {
		return "I am a female";
	}

	@Override
	public String toString() {
		return "" + getClass();
	}
}

class Boy extends Male {

}

class Man extends Male {
	@Override
	public String toString() {
		return "" + getClass();
	}
}

class Girl extends Female {
	@Override
	public String toString() {
		return "" + getClass();
	}
}

class Woman extends Female {
	@Override
	public String toString() {
		return "" + getClass();
	}
}

class PolyInherEncap {
	public static void doSomething(Human h) {
		System.out.println(h.whoAmI());
	}
	public static void main(String[] args) {
		
//		System.out.println("New Human:");
//		Human h = new Human();
//		System.out.println(h.whoAmI());
//		System.out.println(h.toString()+"\n");
		
//		System.out.println("New Human:");
//		Human h = new Boy();
//		System.out.println(h.whoAmI());
//		System.out.println(h.toString()+"\n");
		
//		
//		System.out.println("New Male:");
//		Male m = new Male();
//		System.out.println(m.whoAmI());
//		System.out.println(m.toString()+"\n");
//
//		System.out.println("New Female:");
//		Female f = new Female();
//		System.out.println(f.whoAmI());
//		System.out.println(f.toString()+"\n");
//		
//		System.out.println("New Boy:");
//		Boy b = new Boy();
//		System.out.println(b.whoAmI());
//		System.out.println(b.toString()+"\n");
//		
//		System.out.println("New Man:");
//		Man man = new Man();
//		System.out.println(man.whoAmI());
//		System.out.println(man.toString()+"\n");
//		
//		System.out.println("New Girl:");
//		Girl g = new Girl();
//		System.out.println(g.whoAmI());
//		System.out.println(g.toString()+"\n");
//		
//		System.out.println("New Woman:");
//		Woman w = new Woman();
//		System.out.println(w.whoAmI());
//		System.out.println(w.toString()+"\n");
		doSomething(new Human());
	}
}
