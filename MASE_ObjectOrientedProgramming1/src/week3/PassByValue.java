/**
 * 
 */
package week3;

/**
 * @author A00258349
 *
 */
public class PassByValue {

	static int size = 7;
	public static void main(String[] args) {
		System.out.println(size);
		c1(size);
		System.out.println(size);
		c2(size);
		System.out.println(size);
		PassByValue pbv = new PassByValue();
		c3(pbv, size);
		System.out.println(size);

	}
	public static void c1(int size) {
		size = size + 200;
		System.out.println(size);
	}
	public static void c2(int s) {
		size = s + 200;
		System.out.println(size);
		
	}
	public static void c3(PassByValue pbv, int size) {
	System.out.println(size);
	pbv.size = size + 200;
	}

}
