/**
 * 
 */
package week3;

import java.util.Scanner;

/**
 * @author A00258349
 *
 */
public class MethodExercises {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		methodExercises();

	}

	public static void methodExercises() {

		System.out.println("----- Exercise 1: Sum, Product, Difference, Quotient -----");
		
		System.out.print("X:");
		int x = sc.nextInt();
		System.out.print("Y:");
		int y = sc.nextInt();
		System.out.println("The sum of " + x + " and " + y + " is: " + calculateSum(x, y));
		System.out.println("The product of " + x + " and " + y + " is:" + calculateProduct(x, y));
		System.out.println("The difference of " + x + " and " + y + " is:" + calculateDifference(x, y));
		System.out.println("The quotient " + x + " and " + y + " is:" + calculateQuotient(x, y));
		

		System.out.println("\n----- Exercise 2: Volume of container -----");
		System.out.println("Length:");
		int l = sc.nextInt();
		System.out.println("Width:");
		int w = sc.nextInt();
		System.out.println("Height:");
		int h = sc.nextInt();
		System.out.println("Volume is :" + MethodExercises.calcVolume(l, w, h));
		

		System.out.println("\n----- Exercise 3: Car park -----");
		carPark();

		
		System.out.println("\n----- Exercise 4: Co-ordinate geometry -----");
		coordGeometry();

		
		System.out.println("\n----- Exercise 5: Pythagoras' Thereom -----");
		System.out.println("Opposite:");
		int o = sc.nextInt();
		System.out.println("Adjacent:");
		int a = sc.nextInt();
		System.out.println("Hypotenuse: " + calcHypotenuse(o, a));
		

		System.out.println("\n----- Exercise 6: Powers -----");
		System.out.println("Base:");
		int b = sc.nextInt();
		System.out.println("Exponent:");
		int e = sc.nextInt();
		System.out.println(b + " to the power of " + e + " is: " + intPower(b, e));
		

		System.out.println("\n----- Exercise 7: Sum multiple numbers -----");
		System.out.println("# of numbers: ");
		int n = sc.nextInt();
		System.out.println("The sum of " + n + " values entered is: " + sumTheValues(n));
		

		System.out.println("\n----- Exercise 8: Overloaded methods -----");
		System.out.println("Int to square: ");
		System.out.println("Square: " + square(sc.nextInt()));
		System.out.println("Int to square: ");
		System.out.println("Square: " + square(sc.nextDouble()));
		

		System.out.println("\n----- Exercise 3: Static references -----");
		MethodExercises objRef = new MethodExercises();
		System.out.println("X:");
		x = sc.nextInt();
		System.out.println("Y:");
		y = sc.nextInt();
		System.out.println("Sum: " + objRef.addNumbers(x, y));

		System.out.println("Length:");
		l = sc.nextInt();
		System.out.println("Width:");
		w = sc.nextInt();
		System.out.println("Height:");
		h = sc.nextInt();
		System.out.println("Volume is :" + MethodExercises.calcVolume(l, w, h));

	}

	public int addNumbers(int x, int y) {
		return x + y;
	}

	public static double square(int n) {
		return Math.pow(n, 2);
	}

	public static double square(double n) {
		return Math.pow(n, 2);
	}

	public static int sumTheValues(int n) {
		int sum = 0;
		for (int i = 0; i < n; i++) {
			System.out.println("Enter a number: ");
			sum += sc.nextInt();
		}
		return sum;
	}

	public static double intPower(int b, int e) {
		return Math.pow(b, e);
	}

	public static double calcHypotenuse(int o, int a) {
		return Math.sqrt(Math.pow(o, 2) + Math.pow(a, 2));
	}

	public static double calcDistance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
	}

	public static String calcMidpoint(int x1, int y1, int x2, int y2) {
		return "(" + ((double) (x1 + x2) / 2) + "," + ((double) (y1 + y2) / 2) + ")";
	}

	public static double calcSlope(int x1, int y1, int x2, int y2) {
		return (double) (y2 - y1) / (x2 - x1);
	}

	public static void coordGeometry() {
		System.out.println("X1:");
		int x1 = sc.nextInt();
		System.out.println("Y1:");
		int y1 = sc.nextInt();
		System.out.println("X2:");
		int x2 = sc.nextInt();
		System.out.println("Y2:");
		int y2 = sc.nextInt();

		System.out.println("Distance: " + calcDistance(x1, y1, x2, y2));
		System.out.println("Midpoint: " + calcMidpoint(x1, y1, x2, y2));
		System.out.println("Slope: " + calcSlope(x1, y1, x2, y2));
	}

	public static void carPark() {
		System.out.println("Number of cars:");
		int numCars = sc.nextInt();

		for (int i = 0; i < numCars; i++) {
			System.out.println("Time car #" + (i + 1) + " parked for:");
			double time = sc.nextDouble();
			System.out.println("Price for car #" + (i + 1) + " :" + calculateCharges(time));

		}
	}

	public static double calculateCharges(double time) {
		time = Math.ceil(time);
		if (time >= 8) {
			return 21;
		} else if (time == 1) {
			return 0;
		} else {
			return time * 3;
		}
	}

	public static int calcVolume(int l, int w, int h) {
		return l * w * h;
	}

	public static int calculateSum(int x, int y) {
		return x + y;
	}

	public static int calculateProduct(int x, int y) {
		return x * y;
	}

	public static int calculateDifference(int x, int y) {
		return x - y;
	}

	public static int calculateQuotient(int x, int y) {
		return x / y;
	}

}
