/**
 * 
 */
package week3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author A00258349
 *
 */
public class IterationExercises {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		loopExercises();
	}

	static void loopExercises() {

		// Ex. 1
		// whileLoop1();
		// Ex. 2
		// whileLoop2();
		// Ex. 3
		// whileLoop3();
		// Ex. 4
		// whileLoop4();
		// Ex. 5
		// whileLoop5();
		// Ex. 6
		// whileLoop6();
		// Ex. 7
		// forLoop1();
		// Ex. 8
		// forLoop2();
		// Ex. 9
		// forLoop3();
		// Ex. 10
		// forLoop4();
	}

	static void whileLoop1() {
		int i = 1, sum = 0;
		while (i < 6) {
			System.out.println("Loop1: i == " + i);
			sum += i;
			i++;
		}

		System.out.println("Loop1: Total == " + sum);
		sum = 0;
		i = 1;

		try {
			System.out.println("Enter increment value: ");
			int n = sc.nextInt();
			while (i < 6) {
				System.out.println("Loop1: i == " + i);
				sum += i;
				i += n;
			}
			System.out.println("Loop1: Total == " + sum);
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void whileLoop2() {
		int i = 5, sum = 0;
		while (i > 0) {
			System.out.println("Loop1: i == " + i);
			sum += i;
			i--;
		}

		System.out.println("Loop1: Total == " + sum);
		sum = 0;
		i = 5;

		try {
			System.out.println("Enter decrement value: ");
			int n = sc.nextInt();
			while (i > 0) {
				System.out.println("Loop1: i == " + i);
				sum += i;
				i -= n;
			}
			System.out.println("Loop1: Total == " + sum);
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void whileLoop3() {
		int sum = 0, n = 0;

		try {
			while (n >= 0) {
				sum += n;
				System.out.print("Enter a number: ");
				n = sc.nextInt();
			}
			System.out.println("Total is: " + sum);
			sum = 0;
			n = 0;
			while (true) {
				sum += n;
				System.out.print("Enter a number: ");
				n = sc.nextInt();
				if(n < 0) {
					break;
				}
			}
			System.out.print("Total is: " + sum);
			
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void whileLoop4() {
		int i = 1, sum = 0;

		do {
			System.out.println("Loop1: i == " + i);
			sum += i;
			i++;
		} while (i < 6);

		System.out.println("Loop1: Total == " + sum);
		sum = 0;
		i = 1;

		try {
			System.out.println("Enter increment value: ");
			int n = sc.nextInt();
			do {
				System.out.println("Loop1: i == " + i);
				sum += i;
				i += n;
			} while (i < 6);
			System.out.println("Loop1: Total == " + sum);
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void whileLoop5() {
		int i = 5, sum = 0;
		do {
			System.out.println("Loop1: i == " + i);
			sum += i;
			i--;
		} while (i > 0);

		System.out.println("Loop1: Total == " + sum);
		sum = 0;
		i = 5;

		try {
			System.out.println("Enter decrement value: ");
			int n = sc.nextInt();
			do {
				System.out.println("Loop1: i == " + i);
				sum += i;
				i -= n;
			} while (i > 0);
			System.out.println("Loop1: Total == " + sum);
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void whileLoop6() {
		int sum = 0, n = 0;
		try {
			do {
				sum += n;
				System.out.print("Enter a number: ");
				n = sc.nextInt();
			} while (n >= 0);
			System.out.print("Total is: " + sum);
		} catch (InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
	}

	static void forLoop1() {
		int sum = 0;
		for(int i =0; i <= 20; i++) {
			sum += i;
		}
		System.out.println("Total is: " + sum);
		sum=0;
		for(int i =5; i <= 20; i+=5) {
			sum += i;
		}
		System.out.println("Total is: " + sum);
	}
	
	static void forLoop2() {
		int sum = 0;
		for(int i =40; i >= 30; i--) {
			sum += i;
		}
		System.out.println("Total is: " + sum);
		sum=0;
		for(int i = 40; i >= 30; i-=5) {
			sum += i;
		}
		System.out.println("Total is: " + sum);
	}
	
	static void forLoop3() {
		int sum = 0, n=0;
		try {
			for(int i = 1; i > 0; i++) {
				System.out.print("Enter a number: ");
				n = sc.nextInt();
				if(n < 0) {
					break;
				} else {
					sum += n;
				}
			}
			System.out.println("Total is: " + sum);
		} catch(InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
		
	}
	
	static void forLoop4(){
		int i =0,j=0;
		try {
			System.out.println("Number of rows: ");
			int r = sc.nextInt();
			System.out.println("Number of columns: ");
			int c = sc.nextInt();
			System.out.println("\nUsing for loops: \n");
			for(i = 0; i < r; i++) {
				for(j =0; j < c; j++) {
					System.out.print("*");
				}
				System.out.println("");
			}
			i = r;
			j = c;
			
			System.out.println("\nUsing while loops: \n");
			while(i > 0) {
				while(j > 0) {
					System.out.print("*");
					j--;;
				}
				System.out.println("");
				i--;
				j = c;
			}
		} catch(InputMismatchException e) {
			System.err.println("Invalid input!");
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
