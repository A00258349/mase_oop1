package week2;

import java.util.Scanner;

enum Direction {
	NORTH, SOUTH, EAST, WEST;
}
public class week2 {

	static Scanner sc = new Scanner(System.in);
	static week2 w2 = new week2();

	public static void main(String[] args) {
		// w2.simpleCalcs();
		// w2.compareTwoValues();
		// w2.preAddPostDifference();
		// w2.compareStrings();
		// w2.ternaryOperator();
		// w2.vowelOrConsonant();
		// w2.isMonth();
		// w2.ifGrade();
		// w2.switchMapOperation();
		w2.switchEnumType();
	}

	void simpleCalcs() {
		System.out.println("Enter X: ");
		int x = sc.nextInt();

		System.out.println("Enter Y: ");
		int y = sc.nextInt();

		int sum = x + y;
		int differnce = x - y;
		int product = x * y;
		int quotient = x / y;

		System.out.println("Sum of " + x + " and " + y + " is " + sum);
		System.out.println("Difference between " + x + " and " + y + " is " + differnce);
		System.out.println("Product of " + x + " and " + y + " is " + product);
		System.out.println("Quotient of " + x + " and " + y + " is " + quotient);

	}

	void compareTwoValues() {
		System.out.println("Enter X: ");
		int x = sc.nextInt();

		System.out.println("Enter Y: ");
		int y = sc.nextInt();

		if (x > y) {
			System.out.println("X > Y ");
		} else if (y > x) {
			System.out.println("X < Y ");
		} else if (x == y) {
			System.out.println("X = Y ");
		}
	}

	void preAddPostDifference() {
		int x = 5, y = 10;

		System.out.println("Y = " + y);
		System.out.println("++Y = " + ++y);
		System.out.println("Y++ = " + y++);
		System.out.println("Y = " + y);

		System.out.println("Y = " + y);
		System.out.println("--Y = " + ++y);
		System.out.println("Y-- = " + y++);
		System.out.println("Y = " + y);

	}

	void compareStrings() {
		System.out.println("Enter string s1: ");
		String s1 = sc.next();

		System.out.println("Enter string s2: ");
		String s2 = sc.next();

		System.out.println(s1 + " equivalent to " + s2 + ": " + s1 == s2);

		String name1 = "John"; // string constant pool
		String name2 = new String("John"); // String constant pool

		System.out.println("name1 == name2: " + name1 == name2);
		System.out.println(name1.equals(name2));

		// name2.inter

	}

	void ternaryOperator() {
		boolean isHappy = true;

		String mood = isHappy ? "I am happy" : "I am sad";
		System.out.println(mood);

		int x = 0, y = 0, minVal = 0;

		System.out.println("X:");
		x = sc.nextInt();
		System.out.println("Y:");
		y = sc.nextInt();

		minVal = x < y ? x : y;

		System.out.println(minVal);

	}

	void vowelOrConsonant() {
		System.out.println("enter a char:");
		char letter = sc.next().charAt(0);
		switch (letter) {
		case 'A':
		case 'E':
		case 'I':
		case 'O':
		case 'U':
		case 97:
		case 101:
		case 105:
		case 111:
		case 117:

			break;

		}
	}

	void ifGrade() {
		System.out.println("Enter a mark");
		int mark = -1;

		try {
			mark = sc.nextInt();
			if (mark < 0 || mark > 100) {
				System.err.println("Exception: Mark must be between 0 and 100");
			} else {
				if (mark >= 70) {
					System.out.println("A");
				} else if (mark >= 60) {
					System.out.println("B");
				} else if (mark >= 50) {
					System.out.println("C");
				} else if (mark >= 40) {
					System.out.println("D");
				} else {
					System.out.println("Fail");
				}
			}
		} catch (Exception e) {
			System.err.println("Exception: Unexpected input.");

		}

	}

	void switchMapOperation() {
		System.out.println("Num1:");
		double num1 = sc.nextDouble();
		System.out.println("Num2:");
		double num2 = sc.nextDouble();

		System.out.println("Enter +|-|/|* :");
		char operand = sc.next().charAt(0);
		
		boolean flag = false;

		double ans = 0;

		switch (operand) {
		case '+':
			ans = num1 + num2;
			break;
		case '-':
			ans = num1 - num2;
			break;
		case '/':
			ans = num1 / num2;
			break;
		case '*':
			ans = num1 * num2;
			break;
		default:
			System.err.println("Operand not supported/invalid");
			flag = true;
			break;
		}
		if(!flag) {
			System.out.println(ans);
		}
		

	}

	void switchEnumType() {
		Direction theWay = null;
		
		System.out.println("Do u knoe de wae? (n|s|e|w");
		String choice = sc.next().toUpperCase();
		
		switch(choice) {
		case "N":
			theWay = Direction.NORTH;
			break;
		case "S":
			theWay = Direction.SOUTH;
			break;
		case "E":
			theWay = Direction.EAST;
			break;
		case "W":
			theWay = Direction.WEST;
			break;
		default:
			System.err.println(choice + " is not valid!");
		}
		
		if(theWay != null) {
			switch(theWay) {
			case NORTH:
				System.out.println("Santy");
				break;
			case SOUTH:
				System.out.println("Penguins");
				break;
			case EAST:
				System.out.println("Japan");
				break;
			case WEST:
				System.out.println("Hollywood");
				break;
			}
		}
		
	}
}
