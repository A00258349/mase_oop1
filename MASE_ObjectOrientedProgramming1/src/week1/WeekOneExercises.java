package week1;

import java.util.Scanner;

public class WeekOneExercises {
	
	static Scanner sc = new Scanner(System.in);
	static WeekOneExercises Week1 = new WeekOneExercises();
	
	public static void main(String[] args) {
		
//		if(Week1.didOswaldActAlone()) {
//			System.out.println("He acted alone.");
//		} else {
//			System.out.println("He did not act alone.");
//		}
//		
//		if(Week1.wasOjGuilty()) {
//			System.out.println("He was guilty");
//		} else {
//			System.out.println("He was not guilty");
//		}
//		
//		
//		String filmName = Week1.myFavouriteFilm();
//		System.out.println("Favourite film is: " + filmName);
//		
//		int rating = Week1.filmRating(filmName);
//		System.out.println("Rating is: " + rating);
		
		String a = "rrr";
		String b = a;
		a = "kkkk";
		System.out.println(b);
		
	}

	public boolean didOswaldActAlone() {
		System.out.println("Did Oswald act alone?");
		return sc.nextBoolean();
	}
	
	public boolean wasOjGuilty() {
		System.out.println("Was OJ SImpson guilty?");
		return sc.nextBoolean();
	}
	
	public String myFavouriteFilm() {
		System.out.println("What is your favourite film?");
		sc.nextLine();
		return sc.nextLine();
	}
	
	public int filmRating(String filmName){
		System.out.println("What is the rating of the film?");
		return sc.nextInt();
	}
}
