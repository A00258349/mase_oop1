package week8;

public class MusicExample {

	public static void main(String[] args) {
		
		Instrument[] orchestra = new Instrument[5];
		
		// Part 1
//		orchestra[0] = new Instrument();
//		orchestra[1] = new Instrument();
//		orchestra[2] = new Instrument();
//		orchestra[3] = new Instrument();
//		orchestra[4] = new Instrument();
		
		// Part2
		orchestra[0] = new Wind();
		orchestra[1] = new Percussion();
		orchestra[2] = new Stringed();
		orchestra[3] = new Brass();
		orchestra[4] = new Woodwind();
		
		tuneAll(orchestra);
	}
	public static void tune(Instrument i) {
		i.play();
	}
	public static void tuneAll(Instrument[] orchestra) {
		for(Instrument i : orchestra) {
			tune(i);
		}
	}

}
interface Instrument{
//	public abstract void play() {
//		System.out.println("Instrument is playing");
//	}
//	public abstract void adjust() {
//		System.out.println("Adjusting instrument");
//	}
//	public abstract void what() {
//		System.out.println("Is an instrument");
//	}
	public abstract void play();
	public abstract void adjust();
	public abstract void what();
}
class Stringed implements Instrument{
	@Override
	public void play() {
		System.out.println("Stringed instrument is playing");
	}
	@Override
	public void adjust() {
		System.out.println("Adjusting stringed instrument");
	}
	@Override
	public void what() {
		System.out.println("Is a stringed instrument");
	}
}
class Wind implements Instrument{
	@Override
	public void play() {
		System.out.println("Wind instrument is playing");
	}
	@Override
	public void adjust() {
		System.out.println("Adjusting wind instrument");
	}
	@Override
	public void what() {
		System.out.println("Is a wind instrument");
	}
}
class Percussion implements Instrument{
	@Override
	public void play() {
		System.out.println("Percussion instrument is playing");
	}
	@Override
	public void adjust() {
		System.out.println("Adjusting percussion instrument");
	}
	@Override
	public void what() {
		System.out.println("Is a percussion instrument");
	}
}
class Woodwind extends Wind{
	@Override
	public void play() {
		System.out.println("Woodwind instrument is playing");
	}
	@Override
	public void what() {
		System.out.println("Is a woodwind instrument");
	}
}
class Brass extends Wind{
	@Override
	public void play() {
		System.out.println("Brass instrument is playing");
	}
	@Override
	public void adjust() {
		System.out.println("Adjusting brass instrument");
	}
}
