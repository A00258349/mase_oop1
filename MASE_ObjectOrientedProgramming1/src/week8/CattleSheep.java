package week8;

public class CattleSheep {

	public static void main(String[] args) {
		Cattle cow = new Cattle();
		Sheep ram = new Sheep();
		Dog terrier = new Dog();
		System.out.println("1.");
		if(cow instanceof Tag) {
			System.out.println("Check this animal - cow.");
		}
		if(ram instanceof Tag) {
			System.out.println("Check this animal - ram");
		}
		if(terrier instanceof Tag) {
			System.out.println("Check this animal - terrier");
		}
		
		System.out.println("\n2.");
		vetCheck(cow);
		vetCheck(ram);
		vetCheck(terrier);
	}
	
	static void vetCheck(Animal a) {
		if(a instanceof Tag) {
			System.out.println("Check this animal - "+a.getClass().getSimpleName());
		}
	}
	
}
interface Tag{}
class Animal{	}
class Cattle extends Animal implements Tag{}
class Sheep extends Animal implements Tag{}
class Dog extends Animal{}



