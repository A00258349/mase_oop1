package week7;

public class RightAngledTriangle extends Triangle {
	@Override
	public void draw() {
		System.out.println("Drawing right angled triangle.");
	}
	public void area() {
		System.out.println("Area of right angled triangle.");
	}
}
