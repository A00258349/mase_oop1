package week7;

public class CastingPgm {

	public static void main(String[] args) {
		upcasting();
		downcasting();
		//System.out.print("Reference (-), Object (Triangle): ");
		//progToTheInterface(new Triangle());
		//System.out.print("Reference (RightAngledTriangle), Object (RightAngledTriangle): ");
		//RightAngledTriangle r = new RightAngledTriangle();
		//progToTheInterface(r);
	}
	
	public static void upcasting() {
		System.out.println("A:");
		System.out.print("Reference (Triangle), Object (RightAngledTriangle): ");
		Triangle t1 = new RightAngledTriangle(); // only the draw method is available as the reference type is triangle
		t1.draw(); 
		Triangle t2 = new Triangle();
		RightAngledTriangle r1 = new RightAngledTriangle();
		System.out.println("\nB:");
		System.out.print("Reference (Triangle), Object (Triangle): ");
		t2.draw();
		System.out.print("Reference (Triangle), Object (RightAngledTriangle): ");
		r1.draw();
		t2 = r1;
		t2.draw(); //the output has changed as the object type is now a right angled triangle
	}
	public static void downcasting() {
		System.out.println("\nC:");
		System.out.print("Reference (Shape), Object (Triangle): ");
		Shape shape = new Triangle();
		shape.draw();
		// RightAngledTriangle r1 = (RightAngledTriangle) s1; // This will cause a class cast exception
		
		System.out.println("\n-------Sheet 2----------");
		System.out.println("A: ");
		Triangle t = new Triangle();
		RightAngledTriangle r = new RightAngledTriangle();
		t = r;
		System.out.print("Reference (Triangle), Object (RightAngleTriangle): ");
		t.draw();
		// t.area(); This will not compile as the reference type triangle does not have a method "area()"
		Triangle t1 = new Triangle();
		RightAngledTriangle r1 = new RightAngledTriangle();
		r1 = (RightAngledTriangle) t1; // Cannot downcast without an explicit cast
		
		
		
	}
	public static void progToTheInterface(Shape s) {
		s.draw();
	}

}
